add_requires("tree-sitter")

set_languages("c++20")

target("tree-sitter-lang")
    set_kind("static")
    add_includedirs("src", {public = true})
    add_packages("tree-sitter")
    add_files("src/*.c")
