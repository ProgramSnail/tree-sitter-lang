[
  "let"
  "%"
  "var"
  "$"
] @keyword.storage.type

[
  "in"
  "<-"
  "ref"
  "<>"
  "const"
  "--"
  "out"
  "->"
] @keyword.storage.modifier

[
  "for"
  "@"
] @keyword.control.repeat

[
  "if"
  "??"
  "elif"
  "!!"
  "else"
  "!!=>"
] @keyword.control.conditional

[
  "break"
  "continue"
  "return"
  "bring"
] @keyword.control.return

[
  "do"
  "=>"
  ":="
  "=:"
] @keyword.control

[
  "import"
  "::"
] @keyword.control.import

[
  "->"
  "<-"
  "="
  "|"
  "&"
  ":"
  "!"
  "?"
] @punctuation

[
  ";"
] @punctuation.delimiter

[
  "("
  ")"
  "["
  "]"
  "{"
  "}"
] @punctuation.bracket

(definition_info) @comment.block.documentation
(annotation_info) @comment.block.documentation

(extra) @comment.block

(placeholder) @variable.language
(simple_name_identifier) @variable
(simple_type_identifier) @type
(typeclass_identifier) @tag ; for better highlighting
(argument_name_identifier) @variable.parameter
(argument_type_identifier) @type.builtin ; for better highlighting
(annotation_identifier) @attribute

"\\" @type ; for better highlighting

(operator) @keyword.operator
(operator_tail1) @keyword.operator
(operator_tail2) @keyword.operator
(operator_tail3) @keyword.operator

(float_literal) @constant.numeric.float
(double_literal) @constant.numeric.float
(int_literal) @constant.numeric.integer
(long_literal) @constant.numeric.integer
(index_literal) @constant.numeric.integer
(string_literal) @string
(unicode_string_literal) @string
(char_literal) @constant.char
(unicode_literal) @constant.char
(bool_literal) @constant.language
(unit_literal) @constant.language
(null_literal) @constant.language
