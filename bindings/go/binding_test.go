package tree_sitter_lang_test

import (
	"testing"

	tree_sitter "github.com/smacker/go-tree-sitter"
	"github.com/tree-sitter/tree-sitter-lang"
)

func TestCanLoadGrammar(t *testing.T) {
	language := tree_sitter.NewLanguage(tree_sitter_lang.Language())
	if language == nil {
		t.Errorf("Error loading Lang grammar")
	}
}
