grammar lang ;

// not always most recent grammar
// not checked

source_file: (statement)+ EOF ;

statement: import
         | type_definition
         | function_definition
         | typeclass_definition
         | EXTRA
         ;

import: ('::' | 'import') (SIMPLE_NAME_IDENTIFIER | '_') ('=' SIMPLE_NAME_IDENTIFIER)?
        (':' (SIMPLE_TYPE_IDENTIFIER | SIMPLE_NAME_IDENTIFIER | TYPECLASS_IDENTIFIER | ('(' OPERATOR ')'))+)? ';'
        ;

constraint: '?' expression ;

function_definition: (definietion_info)? (ANNOTATION_INFO)* (constraint ';')*
                     ('.')? (SIMPLE_NAME_IDENTIFIER | ('(' OPERATOR ')')) ('?' | '!')
                     (((ANNOTATION_IDENTIFIER)? (REFERENCE)? ARGUMENT_NAME_IDENTIFIER ('?' | '!')?)*)
                     (':' ((ANNOTATION_IDENTIFIER)? (REFERENCE)? scoped_type)+)?
                     ((/*prec 2*/ array | block) | ('=' super_expression ';')) | ';')
                     ;

type_definition: (DEFINITION_INFO)? (ANNOTATION_INFO)* (REFERENCE)?
                 (SIMPLE_TYPE_IDENTIFIER | TYPECLASS_IDENTIFIER)
                 ('[' (TYPECLASS_IDENTIFIER)+ ']')?
                 (ARGUMENT_TYPE_IDENTIFIER)*
                 ('=' type)?
                 ';'
                 ;

case: (':=' | '=:') expression (('??' | 'if') expression)? (('=>' | 'do') expression)? ;

match: expression (case)+ ;

condition: choice('??' | 'if') expression choice('=>' |'do') expression
           (('!!' | 'elif') expression ('=>' | 'do') expression)*
           (('!!=>' | 'else') _expression)?
           ;

loop: ('@' | 'for') (expression | (expression ':' expression))?,
      ('=>' | 'do') expression
      ;

comma_expression: /* prec left ?? */ super_expression  ','  super_expression ;

operator_expression:  /* prec left ?? */
                   | /* prec 4 */ expression OPERATOR expression
                   | /* prec 3 */ expression OPERATOR_TAIL1 expression
                   | /* prec 2 */ expression OPERATOR_TAIL2 expression
                   | /* prec 1 */ expression OPERATOR_TAIL3 expression
                   ;

block: '{' ((super_expression ';') | EXTRA)* '}' ;

array: '[[' (scoped_expression)+ ']]' ;

name_definition: (('%' | 'let') | ('$' | 'var')) (SIMPLE_NAME_IDENTIFIER | PLACEHOLDER) ;

array_access: scoped_expression '[' super_expression ']' ;

tuple_access: scoped_expression, '.' NUMBER_LITERAL

reference_expression:  /* prec -1 ?? */ REFERENCE scoped_expression ;

suffix_expression: scoped_expression ('?' | '!')

return: ('return' | 'bring') expression ;

loop_control: 'break'
            | 'continue'
            ;

name_expression:
  ((scoped_type '.' SIMPLE_NAME_IDENTIFIER)
  | (scoped_expression '.' SIMPLE_NAME_IDENTIFIER)
  | name_identifier
  | ('(' OPERATOR ')'))
  ((ANNOTATION_IDENTIFIER)? scoped_expression)*
  ;


constructor: scoped_type (ANNOTATION_IDENTIFIER)? scoped_expression)+ ;

lambda: '\\' (ARGUMENT_NAME_IDENTIFIER)* '=>' expression ;

super_expression: match
                | condition
                | loop
                | comma_expression
                | expression
                ;


expression: name_expression
          | operator_expression
          | return
          | lambda
          | constructor
          | not_name_scoped_expression
          ;

scoped_expression: name_identifier
                  | not_name_scoped_expression
                  ;

not_name_scoped_expression: block
                          | array
                          | PLACEHOLDER
                          | name_definition
                          | array_access
                          | tuple_access
                          | loop_control
                          | reference_expression
                          | suffix_expression
                          | literal
                          | '(' super_expression ')'
                          ;

variant_type: ('|')? annotated_type ('|' annotated_type)+ ;

tuple_type: ('&')? annotated_type ('&' _annotated_type)+ ;

annotated_type: (ANNOTATION_IDENTIFIER)? scoped_type ;

reference_type: /* prec -1 */ REFERENCE scoped_type ;

modified_type: scoped_type ('?' | '!') ;

simple_type: type_identifier ('[' (scoped_type)+ ']')? ;

type: simple_type
    | reference_type
    | modified_type
    | variant_type
    | tuple_type
    ;

scoped_type: simple_type
           | reference_type
           | modified_type
           | '(' (variant_type | tuple_type) ')'
           ;

name_identifier: (ARGUMENT_NAME_IDENTIFIER | SIMPLE_NAME_IDENTIFIER) ;
type_identifier: (ARGUMENT_TYPE_IDENTIFIER | SIMPLE_TYPE_IDENTIFIER) ;

literal: FLOAT_NUMBER_LITERAL
       | NUMBER_LITERAL
       | STRING_LITERAL
       | CHAR_LITERAL
       | BOOL_LITERAL
       | UNIT_LITERAL
       | NULL_LITERAL
       ;

DEFINITION_INFO : : ': ' ([^\n]*)+ ;
ANNOTATION_INFO : ANNOTATION_IDENTIFIER [^\n]* ;

EXTRA: EMPTY_LINES
     | LINE_COMMENT
     | LINE_COMMENT
     | BLOCK_COMMENT
     ;

EMPTY_LINES : ('\n')+ ;

EXEC_COMMENT : '#!' [^\n]* -> skip ;
LINE_COMMENT : '//' [^\n]* -> skip ;
BLOCK_COMMENT : '\*' ([^*] |('\*' [^/]))* '*/' -> skip ;

REFERENCE: ('->' | 'out')
         | ('<-' | 'in')
         | ('<>' | 'ref')
         | ('|->' | 'or_out')
         | ('<-|' | 'or_in')
         ;

PLACEHOLDER : '_' ;

SIMPLE_NAME_IDENTIFIER : ([a-z_][a-z0-9_]* '.')* [a-z_][a-z0-9_]* ;

SIMPLE_TYPE_IDENTIFIER : ([a-z_][a-z0-9_]* '.')* [A-Z][a-zA-Z0-9]* ;
TYPECLASS_IDENTIFIER : ([a-z_][a-z0-9_]* '.' )* '#' [A-Z][a-zA-Z0-9]* ;
ARGUMENT_NAME_IDENTIFIER : '\'' [a-z_][a-z0-9_]* ;
ARGUMENT_TYPE_IDENTIFIER : '\'' [A-Z][a-zA-Z0-9]* ;
ANNOTATION_IDENTIFIER : '@' [a-z_][a-z0-9_]* ;

OPERATOR : ([+\-*/%^!?|&,<>=]+)|\.+ ;
OPERATOR_TAIL1 : [+\-*/%^!?|&,<>=]+\. ;
OPERATOR_TAIL2 : [+\-*/%^!?|&,<>=]+\.\. ;
OPERATOR_TAIL3 : [+\-*/%^!?|&,<>=]+\.\.\. ;

FLOAT_NUMBER_LITERAL : '-'? [0-9]+ '.' [0-9]+ 'f' ;
DOUBLE_NUMBER_LITERAL : '-'? [0-9]+ '.' [0-9]+ ;
INT_LITERAL : '-'? [0-9]+ 'i' ;
LONG_LITERAL : '-'? [0-9]+ 'l' ;
INDEX_LITERAL : [0-9]+ ;
STRING_LITERAL : '\"' ([^\\\'] | ( '\\' [\u0000-\u00FF]))* '\"' ;
UNICODE_STRING_LITERAL : '\"' ([^\\\'] | ( '\\' [\u0000-\uFFFF]))* '\"u';
CHAR_LITERAL : '\'\'' ([^\\\'] | ( '\\' [\u0000-\u00FF])) '\'\'' ;
UNICODE_LITERAL : '\'\'' ([^\\\'] | ( '\\' [\u0000-\uFFFF]))  '\'\'u' ;
BOOL_LITERAL : 'true' | 'false' ;
UNIT_LITERAL : '()' ;
NULL_LITERAL : 'null' ;

// IDENTIFIER: ([@'#]?[a-zA-Z0-9_]+)|([+\-*/%^!?|&,<>=]+\.?\.?\.?)|\.+
